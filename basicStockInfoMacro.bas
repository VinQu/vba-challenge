Attribute VB_Name = "Module1"
Sub basicStockInfo()

' Worksheet loop
Dim ws As Worksheet
For Each ws In Worksheets

    ' Variable for stock ticker symbol
    Dim Ticker_Symbol As String
    
    ' Variable for opening Price at beginning of year
    Dim YearOpen_Price As Double
    
    ' Variable for closing price at end of year
    Dim YearClose_Price As Double
    
    ' Variable for price difference at end of year
    Dim YearChange_Price As Double
    
    ' Variable for percent change at end of year
    Dim YearChange_Percent As Double
    
    ' Variable for total stock volume
    Dim Ticker_Total As Double
    
    ' Location of ticker symbol in summary table
    Dim Summary_Table_Row As Integer
    Summary_Table_Row = 2
    
    ' Find out how many rows there are
    Dim Total_Rows As Long
    Total_Rows = ws.Cells(Rows.Count, 1).End(xlUp).Row
    
    ' Set start of year date
    Dim Year_Start As Long
    Year_Start = Application.WorksheetFunction.Min(ws.Columns("B"))
    
    ' Set end of year date
    Dim Year_End As Long
    Year_End = Application.WorksheetFunction.Max(ws.Columns("B"))
    
    'Print summary table headers
    ws.Range("I" & 1).Value = "Ticker"
    ws.Range("J" & 1).Value = "Yearly Change"
    ws.Range("K" & 1).Value = "Percent Change"
    ws.Range("L" & 1).Value = "Total Stock Volume"
    
    ' Loop through all stock ticker symbols
    For i = 2 To Total_Rows
    
        ' Are we still in the same ticker?
        If ws.Cells(i + 1, 1).Value <> ws.Cells(i, 1).Value Then
        
            ' Set the ticker symbol
            Ticker_Symbol = ws.Cells(i, 1).Value
            
            ' Add to the ticker total
            Ticker_Total = Ticker_Total + ws.Cells(i, 7).Value
            
            ' Print ticker symbol
            ws.Range("I" & Summary_Table_Row).Value = Ticker_Symbol
            
            ' Print total stock volume
            ws.Range("L" & Summary_Table_Row).Value = Ticker_Total
             
            ' Last row year end check
            If ws.Cells(i, 2).Value = Year_End Then
               YearClose_Price = ws.Cells(i, 6).Value
            Else
            End If
             
            ' Calculate and print yearly change
            YearChange_Price = YearClose_Price - YearOpen_Price
            ws.Range("J" & Summary_Table_Row).Value = YearChange_Price
            
            ' Conditional formatting
            If YearChange_Price > 0 Then
                ws.Range("J" & Summary_Table_Row).Interior.ColorIndex = 4
                ElseIf YearChange_Price < 0 Then
                    ws.Range("J" & Summary_Table_Row).Interior.ColorIndex = 3
                    Else
                        ws.Range("J" & Summary_Table_Row).Interior.ColorIndex = 2
            End If
                    
            ' Calculate and print yearly percent change
            If YearOpen_Price = 0 Then
                YearChange_Percent = YearClose_Price
            Else
                YearChange_Percent = (YearClose_Price - YearOpen_Price) / YearOpen_Price
            End If
                ws.Range("K" & Summary_Table_Row).Value = YearChange_Percent
                ws.Range("K" & Summary_Table_Row).NumberFormat = "0.00%"
            
            
            'Add one to the summary table row
            Summary_Table_Row = Summary_Table_Row + 1
            
            ' Reset the ticker total stock volume
            Ticker_Total = 0
            
        ' We are still in the same ticker
        Else
        
            ' Add to the ticker total
            Ticker_Total = Ticker_Total + ws.Cells(i, 7).Value
        
            ' Are we at start of year?
            If ws.Cells(i, 2).Value = Year_Start Then
                YearOpen_Price = ws.Cells(i, 3).Value
            Else
            End If
        
            ' Are we at end of year?
            If ws.Cells(i, 2).Value = Year_End Then
                YearClose_Price = ws.Cells(i, 6).Value
            Else
            End If
        
        End If
        
    Next i
    
Next ws

End Sub

